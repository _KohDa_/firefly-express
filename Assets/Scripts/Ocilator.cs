﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ocilator : MonoBehaviour {

    [SerializeField] Vector3 movementVector = new Vector3(10f, 10f, 10f);
    [SerializeField] float period = 2f;

    [Range(0f, 1f)] float movementFactor;

    Vector3 startingPosition;



	// Use this for initialization
	void Start () {
        startingPosition = transform.position;
		
	}
	
	// Update is called once per frame
	void Update () {

        if (period <= Mathf.Epsilon) { return; }
        float cycles = Time.time / period; // grows continually from 0 

        const float tau = Mathf.PI * 2; //about 6.28
        float rawSinWave = Mathf.Sin(cycles * tau);

        movementFactor = rawSinWave / 2 + 0.5f;
        Vector3 offset = movementVector * movementFactor;
        transform.position = startingPosition + offset;
	}
}
