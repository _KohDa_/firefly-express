﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterCollision : MonoBehaviour {

    public ParticleSystem dropsParticles;
    public ParticleSystem splashParticles;
    public int droppletsVolume;

    private List<ParticleCollisionEvent> collisionEvents;


    // Use this for initialization
    void Start()
    {
        collisionEvents = new List<ParticleCollisionEvent>();
    }

	
	// Update is called once per frame
	void Update() {
    }

    public void OnParticleCollision(GameObject other)
    {
        if (other == null) { throw new ArgumentNullException("no game object"); }
        if (collisionEvents == null) { return; } // collisionEvents is null on lvl load?

        ParticlePhysicsExtensions.GetCollisionEvents(dropsParticles, other, collisionEvents);
        for (int i = 0; i < collisionEvents.Count; i++)
        {
            EmitAtLocation(collisionEvents[0]);
            GameObject firefly = GameObject.FindGameObjectWithTag("Hero");
            if (firefly == null) { throw new ArgumentNullException("No firefly"); }
            if(collisionEvents[0].colliderComponent.gameObject == firefly)
            {
                Firefly.hits++;
                print(Firefly.hits);
            }
            
        }
    }

    private void EmitAtLocation(ParticleCollisionEvent particleCollisionEvent)
    {
        Vector3 pos = particleCollisionEvent.intersection;
        Quaternion rot = Quaternion.LookRotation(particleCollisionEvent.normal);
        ParticleSystem splashHere = Instantiate(splashParticles, pos, rot);
        splashHere.transform.parent = gameObject.transform; //child object to gameobject so that it can be destroyed here
        splashHere.Emit(droppletsVolume);

        DestroySplash(splashHere);
    }

    private void DestroySplash(ParticleSystem particleSystem)
    {
        ParticleSystem.MainModule main = particleSystem.main;
        var splashStartTime = main.startLifetime;
        float splashDuration = main.duration + splashStartTime.constantMax;
        Destroy(particleSystem, splashDuration);
    }
}
