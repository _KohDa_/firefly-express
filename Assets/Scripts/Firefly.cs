﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Firefly : MonoBehaviour {

    Rigidbody fireFlyRigidBody;
    AudioSource audioSource;
    AudioSource boostAudio;
    [SerializeField] float velocity = 10f; //vertical thrust
    [SerializeField] float rotationalThrust = 200f; 
    [SerializeField] float selfRightingSpeed = .5f; // Bigger for slower
    [SerializeField] float flickerSpeed = 0.1f;

    [SerializeField] ParticleSystem boostParticles;
    [SerializeField] ParticleSystem hitParticles;
    //[SerializeField] ParticleSystem deathParticles; //not using death particles but a bulb flash
    [SerializeField] ParticleSystem levelWonParticles;

    [SerializeField] AudioClip boostSound;
    [SerializeField] AudioClip hitSound;
    [SerializeField] AudioClip dieSound;
    [SerializeField] AudioClip levelWonSound;

    [SerializeField] byte maxHits = 3;
    public static byte hits;

    private static int currentSceneIndex;
    private static int nextSceneIndex;

    //for flicker
    GameObject fireFlyButt;
    MeshRenderer fireFlyLight;

    private enum State { Alive, Dead, Transcending};
    State state = State.Alive;

    // Use this for initialization
    void Start () {
        hits = 0;
        fireFlyRigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        boostAudio = GetComponent<AudioSource>();

        //for flickering light of death
        fireFlyButt = GameObject.FindWithTag("FireFlyButt");
        fireFlyLight = fireFlyButt.GetComponent<MeshRenderer>();

        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        nextSceneIndex = currentSceneIndex + 1;
    }
    

    // Update is called once per frame
    void Update ()
    {
        if (state == State.Alive)
        {  ProcessInput();
            CheckIfDying();
        }
        
    }

    private void ProcessInput()
    {
        RespondToThrust();
        RespondToRotate();
    }

    private void OnCollisionEnter(Collision collision) 
    {
        if (state != State.Alive) { return;}

        boostAudio.Stop();
        switch (collision.gameObject.tag) 
        {
            case "Friendly":
                break;
            case "Fuel":
                print("Fuel");
                break;
            case "Finish":
                StartWinSequence();
                break;
            case "FireFlyButt":
                break;
            default:
                ManageHits();
                break;            
        }
    }

    private void StartWinSequence()
    {
        state = State.Transcending;
        audioSource.PlayOneShot(levelWonSound);
        levelWonParticles.Play();
        Invoke("LoadNextScene", levelWonSound.length);
    }

    public void ManageHits()
    {
        hits++;
        CheckIfDying();
        if (state != State.Dead)
        {
            HitObject();
        }
    }

    public void HitObject()
    {
        audioSource.PlayOneShot(hitSound);
        ParticleSystem smash = Instantiate(hitParticles, transform.position, transform.rotation);
        smash.Play();
    }

    public void CheckIfDying()
    {
        if (hits >= maxHits)
        {
            state = State.Dead;
            StartDeathSequence();
        }
    }

    private void StartDeathSequence()
    {
        audioSource.PlayOneShot(dieSound);
        Invoke("LoadNextScene", dieSound.length);
        StartCoroutine(DeathFlicker());
    }

    public IEnumerator DeathFlicker()
    {
        int deathFlickers = 6;
        //Color lightOn = fireFlyLight.GetComponent<Color>();
        //Color lightOff = Color.black;

        for (var i = 0; i < deathFlickers; i++)
        {
            //fireFlyLight.material.color = lightOn;
            fireFlyLight.enabled = true;
            yield return new WaitForSeconds(flickerSpeed);
            //fireFlyLight.material.color = lightOff;
            fireFlyLight.enabled = false;
            yield return new WaitForSeconds(flickerSpeed);
        }

    }

    private void LoadNextScene()
    {
        switch (state)
        {
            case State.Alive:
                //dosomething
                break;
            case State.Dead:
                SceneManager.LoadScene(currentSceneIndex);
                break;
            case State.Transcending:
                SceneManager.LoadScene(nextSceneIndex);
                break;
        }
    }
       

    private void RespondToThrust()
    {
        if (Input.GetKey(KeyCode.Space))
        //force relative to rotation
        {
            if (!boostAudio.isPlaying)
            { boostAudio.PlayOneShot(boostSound); }

            if(!boostParticles.isEmitting)
            { boostParticles.Play(); }

            fireFlyRigidBody.AddRelativeForce(new Vector3(0f, velocity, 0f));
        }
        else
        {
            //boostParticles.Stop();
            boostAudio.Stop(); }
    }

    private void RespondToRotate()
    {
        fireFlyRigidBody.freezeRotation = true; //take manual control of rotation
        bool canTilt = TiltFirefly();

        float rotationPerFrame = rotationalThrust * Time.deltaTime;

        if (canTilt && Input.GetKey(KeyCode.RightArrow))
        { 
            transform.Rotate(-Vector3.forward * rotationPerFrame);
        }
        else if (canTilt && Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.forward * rotationPerFrame);
        } else { RightFirefly(); }
            

        fireFlyRigidBody.freezeRotation = false; //resume physics control of rotation
    }

    private void RightFirefly()
    {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.identity, Time.deltaTime / selfRightingSpeed);
    }

    private bool TiltFirefly()
    {
        float maxBackward = -0.4f;
        float maxForward = 0.4f;
        float currentTiltZAxis = transform.rotation.z;
        

        bool canTilt = (currentTiltZAxis > maxBackward && currentTiltZAxis < maxForward) ? true : false;
        return canTilt;
    }
}
